<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "Nama Hewan : " . $sheep->nama; // "shaun"
echo "<br>";
echo "Jumlah kaki : " . $sheep->legs; // 4
echo "<br>";
echo "Apakah Berdarah dingin ? " . $sheep->cold_blooded; // "no"
echo "<br><br>";


$kodok = new frog("Keropiii");
echo "Nama Hewan : " . $kodok->nama; // "shaun"
echo "<br>";
echo "Jumlah kaki : " . $kodok->legs; // 4
echo "<br>";
echo "Apakah Berdarah dingin ? " . $kodok->cold_blooded; // "no"
echo "<br>";
echo $kodok -> jump();
echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Nama Hewan : " . $sungokong->nama; // "shaun"
echo "<br>";
echo "Jumlah kaki : " . $sungokong->legs; // 4
echo "<br>";
echo "Apakah Berdarah dingin ? " . $sungokong->cold_blooded; // "no"
echo "<br>";
echo $sungokong -> yell();
echo "<br><br>";


?>