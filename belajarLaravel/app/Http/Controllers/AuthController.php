<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function login(Request $request){
        $namaDepan = $request -> input("firstname");
        $namaBelakang = $request -> input("lastname");
        return view('home', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
