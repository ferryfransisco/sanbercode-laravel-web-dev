@extends('layout.master')
@section('title')
Registration Page
@endsection

@section('sub-title')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/home" method="POST">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" name="firstname" id="fname"><br><br>
        <label for="lname">Last name:</label><br>
        <input type="text" name="lastname" id="lname"><br><br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female <br>
        <input type="radio" name="gender" value="3">Other <br><br>
        <label>Nationality:</label> <br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Japanese</option>
            <option value="3">Korean</option>
            <option value="4">Chinese</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" value="1" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" value="2" name="bahasa">English<br>
        <input type="checkbox" value="3" name="bahasa">Other<br><br>
        <label>Bio:</label> <br>
        <textarea name="biolengkap" id="" cols="25" rows="10"></textarea><br><br>
       
        <input type="submit" value="Sign Up" >
    </form>

@endsection
