<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome', [
        "title" => "Homepage",
    ]);
});

Route::get('/about', function(){
    return view('about', [
        "title" => "About",
    ]);
});
Route::get('/product1', function(){
    return view('product1',[
        "title" => "Headset 1",
    ]);
});