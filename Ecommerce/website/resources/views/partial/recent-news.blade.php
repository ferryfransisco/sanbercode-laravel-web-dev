<section class="best-product container">
    <h2 class="letter-spacing bold-800 fs-poppins">Recent News</h2>
    <p class="fs-montserrat fs-100">Read the news from technology</p>
  </section>
  <!-- ============Recent News=========================== -->

  <section class="recent-news grid">
    <div class="news grid">
      <img src="image/news-1.png" alt="" />
      <div class="fs-montserrat fs-100 flex padding-inline">
        <p>October 5, 2022</p>
        <p>by Store1/ Edit</p>
      </div>
      <h2 class="fs-poppins padding-inline fs-200 blod-600">
        How to choose perfect gadgets
      </h2>
      <p class="fs-montserrat padding-inline fs-100">
        When, while the lovely valley teems with vapour around me, and the
        meridian sun strikes the upper s ...
      </p>
    </div>

    <div class="news grid">
      <img src="image/news-2.png" alt="" />
      <div class="fs-montserrat fs-100 flex padding-inline">
        <p>October 5, 2022</p>
        <p>by User3/ Edit</p>
      </div>
      <h2 class="fs-poppins padding-inline fs-200 blod-600">
        How to choose perfect gadgets
      </h2>
      <p class="fs-montserrat padding-inline fs-100">
        When, while the lovely valley teems with vapour around me, and the
        meridian sun strikes the upper s ...
      </p>
    </div>

    <div class="news grid">
      <img src="image/news-1.png" alt="" />
      <div class="post-date fs-montserrat fs-100 flex padding-inline">
        <p>October 5, 2022</p>
        <p>by User1/ Edit</p>
      </div>
      <h2 class="fs-poppins padding-inline fs-200 blod-600">
        How to choose perfect gadgets
      </h2>
      <p class="fs-montserrat padding-inline fs-100">
        When, while the lovely valley teems with vapour around me, and the
        meridian sun strikes the upper s ...
      </p>
    </div>
  </section>